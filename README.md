# ci-exit-status-sandbox

Project for exploring how CI exit statuses work with respect to `set -e` in bash.
 
Used to explore behavior related to https://gitlab.com/gitlab-org/gitlab-runner/-/issues/25394

See `.gitlab-ci.yml` and `test-bash-failure-behavior`.


# Details:

`set -e` is set automatically in the GitLab CI environment.  You might expect
the 'echo' command below in the CI script to not be executed, but it is, and the corresponding
bash script `test-bash-failure-behavior` which has identical commands
shows that GitLab CI behaves in the same way as a normal bash script in this respect.

For the reason behind this behavior of `set -e`, see:
https://www.gnu.org/software/bash/manual/html_node/The-Set-Builtin.html

Specifically:

> The shell does not exit if the command that fails is ... part of any command
> executed in a && or || list except the command following the final && or ||...

For more context on the many other unexpected and inconsistent behaviors around `set -e`, see:

https://mywiki.wooledge.org/BashFAQ/105

`.gitlab-ci.yml`:
```yaml
test-bash-failure-behavior:
  stage: build
  script:
    - nonexistent-failing-command && true
    - echo "THIS ECHO WILL STILL PRINT BECAUSE OF THE WAY 'set -e' WORKS"
```

`test-bash-failure-behavior`:
```bash
#!/usr/bin/env bash

set -e
nonexistent-failing-command && true
echo "THIS ECHO WILL STILL PRINT BECAUSE OF THE WAY 'set -e' WORKS"
```
